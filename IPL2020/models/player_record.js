const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const paginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    fixture_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"fixture",
    },
    player_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"player",
    },
    runs: {
        type: Number,
        default:0,
        required: true
    }


},{usePushEach: true});
schema.plugin(paginate);
schema.plugin(timestamps);

module.exports = mongoose.model('player_record', schema);