const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const paginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"User"
    },
    date: {
        type: Date,
        required: true
    },
    teams: [{
        type: mongoose.Schema.Types.ObjectId,
        ref:"team"
    }],
    playing_players: [{
        type: mongoose.Schema.Types.ObjectId,
        ref:"player"
    }]


},{usePushEach: true});
schema.plugin(paginate);
schema.plugin(timestamps);

module.exports = mongoose.model('fixture', schema);