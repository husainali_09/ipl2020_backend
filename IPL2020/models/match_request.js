const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const paginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    admin_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    fixture_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "fixture",
        required: true
    },
    bid: {
        type: Number,
        default: 0,
        required: true
    }
}, { usePushEach: true });
schema.plugin(paginate);
schema.plugin(timestamps);
module.exports = mongoose.model('match_request', schema);