const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const paginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"User"
    },
    team_name: {
        type: String,
        required: true,
        unique: true
    },
    logo_url: {
        type: String
    },
    player_id: [{
        type: mongoose.Schema.Types.ObjectId,
        ref:"player"
    }]
},{usePushEach: true});
schema.plugin(paginate);
schema.plugin(timestamps);

module.exports = mongoose.model('team', schema);