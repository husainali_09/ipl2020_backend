const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const paginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    fixture_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"fixture",
        required: true

    },
    admin_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"User",
        required: true

    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"User",
        required: true
    },
    admin_team: [{
        player_id : {
            type: mongoose.Schema.Types.ObjectId,
            ref:"player",
        },
        record_id : {
            type: mongoose.Schema.Types.ObjectId,
            ref:"record",
        },
    }],
    user_team: [{
        player_id : {
            type: mongoose.Schema.Types.ObjectId,
            ref:"player",
        },
        record_id : {
            type: mongoose.Schema.Types.ObjectId,
            ref:"record",
        },

    }],
    admin_score: {
        type: Number,
        default:0
    },
    user_score: {
        type: Number,
        default:0
    },
    bid: {
        type: Number,
        required: true
    }
},{usePushEach: true});
schema.plugin(paginate);
schema.plugin(timestamps);

module.exports = mongoose.model('match_detail', schema);