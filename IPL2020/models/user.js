const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const mongooseHidden = require('mongoose-hidden')();
const timestamps = require('mongoose-timestamp');
const paginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    name: {
        type: String,
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        select: true
    },
    scope:[{
        type:Number,
        required: true
    }]
},{usePushEach: true});
schema.plugin(paginate);
schema.plugin(timestamps);
// schema.plugin(mongooseHidden,{ hidden: {_id:false, password: true,isActive:true,createdAt:true,updatedAt:true} });


schema.pre('save', function(next) {

    let user = this;
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(10, function(err, salt) {
        if (err) return next(err);

        // hash the password along with our new salt
        console.log(user.password,'_____________________________');
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

schema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        console.log(isMatch);
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', schema);