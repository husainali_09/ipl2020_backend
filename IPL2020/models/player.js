const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const paginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"User"
    },
    player_name: {
        type: String,
        required: true
    },
    team_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"team"
    },
    total_runs: {
        type: Number,
        default: 0
    },
    player_type: {
        type: String,
        require: true
    }
},{usePushEach: true});
schema.plugin(paginate);
schema.plugin(timestamps);


module.exports = mongoose.model('player', schema);