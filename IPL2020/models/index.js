module.exports = {
    'User' :require('./user'),
    'team' :require('./team'),
    'player' : require('./player'),
    'fixture' :require('./fixture'),
    'match_detail' :require('./match_detail'),
    'player_record' : require('./player_record'),
    'match_request' : require('./match_request')

};