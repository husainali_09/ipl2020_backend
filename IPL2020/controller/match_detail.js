const db = require('../models/index');

module.exports = {

    createMatchDetailFunction: async (req, callback) => {
        try {
            const { admin_id, fixture_id, bid } = req.body;
            const user_id = req.user;
            await db['match_detail'].create({
                user_id: user_id,
                admin_id: admin_id,
                fixture_id: fixture_id,
                bid: bid
            });
            callback(201, { message: "Match Details Added", status: 201, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    getMatchDetailFunction: async (req, callback) => {
        try {
            const res = await db['match_detail'].find();
            callback(201, { message: res, status: 200, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    deleteMatchDetailFunction: async (req, callback) => {
        try {
            const res = await db['match_detail'].deleteOne({_id : req.params.match_detail_id });
            console.log(res);
            if (res.deletedCount === 1) {
                callback(200, { message: "Match Details Deleted", status: 200, response: null });
            } else {
                callback(400, { message: "Match Details not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }

    }

};