const db = require('../models/index');

module.exports = {

    createRecordFunction: async (req, callback) => {
        try {
            await db['player_record'].create(req.body);

            // And Will Add Total Runs in Tournament for a player so far, in Player module
            const res = await db['player'].find({_id: req.body.player_id},{total_runs:1, _id:0});
            let totalRuns = res[0].total_runs + req.body.runs;
            await db['player'].update({_id: req.body.player_id}, {total_runs: totalRuns});

            callback(201, { message: "Record Added", status: 201, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    getRecordFunction: async (req, callback) => {
        try {
            const res = await db['player_record'].find();
            callback(201, { message: res, status: 200, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    updateRecordFunction: async (req, callback) => {
        try {
            const record_id = req.params.player_record_id;
            const filter = { _id: record_id };
            const update = req.body
            const res = await db['player_record'].update(filter, update);
            if (res.nModified !== 0) {
                callback(200, { message: "Record Updated", status: 200, response: null });
            } else {
                callback(400, { message: "Record not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }

    },

    deleteRecordFunction: async (req, callback) => {
        try {
            const res = await db['player_record'].deleteOne({_id : req.params.player_record_id });
            console.log(res);
            if (res.deletedCount === 1) {
                callback(200, { message: "Record Deleted", status: 200, response: null });
            } else {
                callback(400, { message: "Record not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }

    }

};