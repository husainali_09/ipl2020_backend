const db = require('../models/index');
const constant = require('../constant');

module.exports = {

    createUserFunction : (req, callback) => {
        req.body.scope = constant.scope.USER
        db['User'].create(req.body)
            .then(res => {
                callback(201, { message: "User created.", status: 201, response: null })
            })
            .catch(err => {
                console.log(err);
                callback(500, { message: "Something went wrong.", status: 500, error: err })
            })
    },

    getUserFunction : (req, callback) => {
        req.body.scope = constant.scope.USER
        db['User'].find()
            .then(res => {
                callback(201, { message: res, status: 200, response: null })
            })
            .catch(err => {
                console.log(err);
                callback(500, { message: "Something went wrong.", status: 500, error: err })
            })
    },

    updateUserFunction: async (req, callback) => {
        try {
            const user_id = req.user;
            const filter = { _id: user_id };
            const update = req.body
            const res = await db['User'].update(filter, update);
            if (res.nModified !== 0) {
                callback(200, { message: "User Updated", status: 200, response: null });
            } else {
                callback(400, { message: "User not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }

    },

    deleteUserFunction: async (req, callback) => {
        try {
            const res = await db['User'].deleteOne({_id : req.user });
            console.log(res);
            if (res.deletedCount === 1) {
                callback(200, { message: "User Deleted", status: 200, response: null });
            } else {
                callback(400, { message: "User not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }

    }

};

function EncodeBase64(detail, location) {
    return new Promise(function (resolve, reject) {

        let type = detail.filetype;
        let typeArray = type.split("/");

        let base64 = detail.base64;
        let fileType = typeArray[1];
        let mediaLocation = location + '.' + fileType;
        fs.writeFile('./' + mediaLocation, new Buffer(base64, 'base64'), function (err) {
            if (err) {
                console.log(err);
                reject(err)
            } else {
                console.log("no error");
                resolve(mediaLocation);
            }
        });
    })
}