const db = require('../models/index');

module.exports = {

    createPlayerFunction: async (req, callback) => {
        try {
            let { player_name, player_type } = req.body;
            let user_id = req.user;
            await db['player'].create({
                player_name: player_name,
                player_type: player_type,
                user_id: user_id
            });
            callback(201, { message: "Player Added", status: 201, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    getPlayerFunction: async (req, callback) => {
        try {
            const res = await db['player'].find();
            callback(201, { message: res, status: 200, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    updatePlayerFunction: async (req, callback) => {
        try {
            const player_id = req.params.player_id;
            const filter = { _id: player_id };
            const update = req.body
            const res = await db['player'].update(filter, update);
            if (res.nModified !== 0) {
                callback(200, { message: "Player Updated", status: 200, response: null });
            } else {
                callback(400, { message: "Player not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    deletePlayerFunction: async (req, callback) => {
        try {
            const player_id = req.params.player_id;
            const res = await db['player'].deleteOne({ _id: player_id });
            if (res.deletedCount === 1) {
                callback(200, { message: "Player Deleted", status: 200, response: null });
            } else {
                callback(400, { message: "Player not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    }
};