const db = require('../models/index');

module.exports = {

    createFixtureFunction: async (req, callback) => {
        try {
            const { date, teams } = req.body;
            const user_id = req.user;
            await db['fixture'].create({
                date: date,
                teams: teams,
                user_id: user_id
            });
            callback(201, { message: "Fixture Added", status: 201, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    getFixtureFunction: async (req, callback) => {
        try {
            const res = await db['fixture'].find();
            callback(201, { message: res, status: 200, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    updateFixtureFunction: async (req, callback) => {
        try {
            const fixture_id = req.params.fixture_id;
            const filter = { _id: fixture_id };
            const update = req.body
            const res = await db['fixture'].update(filter, update);
            if (res.nModified !== 0) {
                callback(200, { message: "Fixture Updated", status: 200, response: null });
            } else {
                callback(400, { message: "Fixture not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }

    },

    deleteFixtureFunction: async (req, callback) => {
        try {
            const res = await db['fixture'].deleteOne({_id : req.params.fixture_id });
            console.log(res);
            if (res.deletedCount === 1) {
                callback(200, { message: "Fixture Deleted", status: 200, response: null });
            } else {
                callback(400, { message: "Fixture not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }

    }

};