const db = require('../models/index');

module.exports = {

    createMatchRequestFunction: async (req, callback) => {
        try {
            const { admin_id, fixture_id, bid } = req.body;
            const user_id = req.user;
            await db['match_request'].create({
                user_id: user_id,
                admin_id: admin_id,
                fixture_id: fixture_id,
                bid: bid
            });
            callback(201, { message: "Request Added", status: 201, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    getMatchRequestFunction: async (req, callback) => {
        try {
            const res = await db['match_request'].find({});
            callback(201, { message: res, status: 200, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    deleteMatchRequestFunction: async (req, callback) => {
        try {
            const res = await db['match_request'].deleteOne({_id : req.params.match_request_id });
            console.log(res);
            if (res.deletedCount === 1) {
                callback(200, { message: "Request Deleted", status: 200, response: null });
            } else {
                callback(400, { message: "Request not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }

    }

};