const db = require('../models/index');

module.exports = {

    createTeamFunction: async (req, callback) => {
        try {
            const { team_name, logo_url, player_id } = req.body;
            const user_id = req.user;
            await db['team'].create({
                team_name: team_name,
                logo_url: logo_url,
                player_id: player_id,
                user_id: user_id
            });

            //Will Set Team, corresponding to players in player model.
            const res = await db['team'].find({ team_name: team_name }, { _id: 1 });
            await db['player'].update({ _id: { $in: player_id } }, { team_id: res[0]._id });

            callback(201, { message: "Team Added", status: 201, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    getTeamFunction: async (req, callback) => {
        try {
            const res = await db['team'].find();
            callback(201, { message: res, status: 200, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    updateTeamFunction: async (req, callback) => {
        try {
            const team_id = req.params.team_id;
            const filter = { _id: team_id };
            const update = req.body
            const res = await db['team'].update(filter, update);
            if (res.nModified !== 0) {
                callback(200, { message: "Team Updated", status: 200, response: null });
            } else {
                callback(400, { message: "Team not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }

    },

    deleteTeamFunction: async (req, callback) => {
        try {
            const res = await db['team'].deleteOne({ _id: req.params.team_id });
            if (res.deletedCount === 1) {
                callback(200, { message: "Team Deleted", status: 200, response: null });
            } else {
                callback(400, { message: "Team not exist ", status: 400, response: null });
            }

        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }

    },

    getAllPlayersByTeamIdFunction: async (req, callback) => {
        try {
            const res = await db['team'].find({ _id: req.params.team_id }, { player_id: 1, _id: 0 });
            callback(201, { message: res, status: 200, response: null });
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    updateTeamPlayersFunction: async (req, callback) => {
        try {
            const res = await db['team'].updateOne({
                _id: req.params.team_id
            }, {
                $push: { player_id: req.body.player_id }
            });
            if (res.nModified !== 0) {
                callback(200, { message: "Player Updated in Team", status: 200, response: null });

                //Will Set Team, corresponding to players in player model.
                const res = await db['team'].find({ team_name: team_name }, { _id: 1 });
                await db['player'].update({ _id: { $in: player_id } }, { team_id: res[0]._id });
            } else {
                callback(400, { message: "Team not exist", status: 400, response: null });
            }
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    },

    deleteTeamPlayerFunction: async (req, callback) => {
        try {
            const res = await db['team'].updateOne({
                _id: req.params.team_id
            }, {
                $pull: { player_id: req.body.player_id }
            });
            if (res.nModified < 0) {
                callback(200, { message: "Player Updated in Team", status: 200, response: null });

                //Will Set Team, corresponding to players in player model.
                await db['player'].update({ _id: { $in: player_id } }, { team_id: null });
            } else {
                callback(400, { message: "Team not exist", status: 400, response: null });
            }
        } catch (err) {
            console.log(err);
            callback(500, { message: "Something went wrong.", status: 500, error: err });
        }
    }

};