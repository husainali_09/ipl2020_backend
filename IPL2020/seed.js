// const db = require("./models/index.js");
const constant = require('./constant');
const models = require('./models/oauth');
const db = require('./models');

module.exports = {
    runSeed : function(){
        models['OAuthClientsModel'].find({}).then(client=>{
            if (client.length <= 0){
                models["OAuthClientsModel"].create({
                    clientId : "DEMO",
                    clientSecret : "DEMO123"
                });

                db['user'].create({
                    email : "admin@gmail.com",
                    password : "123",
                    scope : constant.scope.ADMIN
                });

            }
        }).catch(err=>{
            console.log(err);
        });

    }
};

