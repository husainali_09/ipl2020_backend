let express = require("express");

module.exports = function(app) {

    const router = express.Router();
    require('../routes/oauth')(app, router);
    require('../routes/users')(app, router);
    require('../routes/player')(app, router);
    require('../routes/fixture')(app, router);
    require('../routes/team')(app, router);
    require('../routes/match_request')(app, router);
    require('../routes/match_detail')(app, router);
    require('../routes/player_record')(app, router);

    app.use('/public', express.static('public'));
    app.use('/v1',router);
};