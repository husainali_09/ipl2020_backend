const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('./config');
const oauthServer = require('oauth2-server');
const Request = oauthServer.Request;
const Response = oauthServer.Response;
const mongoose = require('mongoose');
const app = require('express')();
const server = require('http').createServer(app);
const seedFunction = require('./seed');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/IPL2020');

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({ limit: '100mb', type: 'application/json' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }));
app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Credentials', false);
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Authorization, Cache-Control, Content-Type');
    if ('OPTIONS' == req.method) return res.status(200).send();
    next();
});
app.oauth = new oauthServer({
    model: require('./models/oauth'),
    // accessTokenLifetime: 20
});
app.authenticate = function (option) {
    let options = option || {};
    return function (req, res, next) {
        let request = new Request({
            headers: { authorization: req.headers.authorization },
            method: req.method,
            query: req.query,
            body: req.body
        });
        let response = new Response(res);
        app.oauth.authenticate(request, response, options)
            .then(function (token) {
                // Request is authorized.
                req.user = token.user._id;

                next()

            })
            .catch(function (err) {
                // Request is not authorized.
                res.status(err.code || 500).json(err)
            });
    }
};

require("./config/routes")(app);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    console.log(err);
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.send(err);
});

server.listen(config.PORT, function () {
    seedFunction.runSeed();
    console.log("----server started---- on port=>", config.PORT);
});

