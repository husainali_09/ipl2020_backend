const matchDetailControllers = require('../controller/match_detail');
const constant = require('../constant');

module.exports = (app, router, client) => {

    router.route('/match_details')
        .post(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            matchDetailControllers.createMatchDetailFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .get(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            matchDetailControllers.getMatchDetailFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
    router.route('/match_details/:match_detail_id')
        .delete(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            matchDetailControllers.deleteMatchDetailFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
};