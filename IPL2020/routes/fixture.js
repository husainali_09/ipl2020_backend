const fixtureController = require('../controller/fixture');
const constant = require('../constant');

module.exports = (app, router, client) => {

    router.route('/fixtures')
        .post(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            fixtureController.createFixtureFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .get(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            fixtureController.getFixtureFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
    router.route('/fixtures/:fixture_id')
        .put(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            fixtureController.updateFixtureFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .delete(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            fixtureController.deleteFixtureFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
};