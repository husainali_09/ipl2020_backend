const playerController = require('../controller/player');
const constant = require('../constant');

module.exports = (app, router, client) => {

    router.route('/players')
        .post(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            playerController.createPlayerFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .get(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            playerController.getPlayerFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
    router.route('/players/:player_id')
        .put(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            playerController.updatePlayerFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .delete(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            playerController.deletePlayerFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })

};