const teamController = require('../controller/team');
const constant = require('../constant');

module.exports = (app, router, client) => {

    router.route('/teams')
        .post(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            teamController.createTeamFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .get(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            teamController.getTeamFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
    router.route('/teams/:team_id')
        .put(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            teamController.updateTeamFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .delete(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            teamController.deleteTeamFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
    router.route('/teams/players/:team_id')
        .get(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            teamController.getAllPlayersByTeamIdFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .patch(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            teamController.updateTeamPlayersFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .delete(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            teamController.deleteTeamPlayerFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })

};