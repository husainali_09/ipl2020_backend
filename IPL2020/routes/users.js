const userController = require('../controller/user-controller');
const constant = require('../constant');

module.exports = (app, router, client) => {

    router.route('/users')
        .post(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            userController.createUserFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .get(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            userController.getUserFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .put(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            userController.updateUserFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .delete(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            userController.deleteUserFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
};