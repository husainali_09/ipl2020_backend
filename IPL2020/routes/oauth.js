const express = require('express');
const oauthServer = require('oauth2-server');
const Request = oauthServer.Request;
const Response = oauthServer.Response;

module.exports = function(app) {
    app.all('/login', function(req,res,next){
        let request = new Request(req);
        let response = new Response(res);
        app.oauth
            .token(request,response)
            .then(function(token) {
                // Todo: remove unnecessary values in response
                return res.json(token)
            }).catch(function(err){
            return res.status(400).json(err)
        })
    });
};
