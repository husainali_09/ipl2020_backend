const recordController = require('../controller/player_record');
const constant = require('../constant');

module.exports = (app, router, client) => {

    router.route('/player_records')
        .post(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            recordController.createRecordFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .get(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            recordController.getRecordFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
    router.route('/player_records/:player_record_id')
        .put(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            recordController.updateRecordFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .delete(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            recordController.deleteRecordFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })

};