const matchRequestController = require('../controller/match_request');
const constant = require('../constant');

module.exports = (app, router, client) => {

    router.route('/match_requests')
        .post(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            matchRequestController.createMatchRequestFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
        .get(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            matchRequestController.getMatchRequestFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
    router.route('/match_requests/:match_request_id')
        .delete(app.authenticate([constant.scope.ADMIN]), function (req, res) {
            matchRequestController.deleteMatchRequestFunction(req, function (status, data) {
                res.status(status).send(data);
            })
        })
};